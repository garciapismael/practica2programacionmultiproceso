import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ejercicio2 {
    public static void main(String[] args) throws IOException {
        //Llama a Random10.jar.
        String command = "java -jar out\\artifacts\\Random10_jar\\Random10.jar";
        List<String> argList = new ArrayList<>(Arrays.asList(command.split(" ")));
        //Crea los processos.
        ProcessBuilder pb = new ProcessBuilder(argList);
        Process process = pb.start();
        //El bufer para escribir los numeros en el random.txt.
        BufferedWriter bw1 = new BufferedWriter(new FileWriter("randoms.txt"));

        OutputStream os = process.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os);
        BufferedWriter bw = new BufferedWriter(osw);
        //Scaner para leer lo que imprime la clase Random10
        Scanner procesoSC = new Scanner(process.getInputStream());
        Scanner sc = new Scanner(System.in);
        String cadena = sc.nextLine().toLowerCase();
        String numeros = "";

        try{
            //compara la cadena que has escrito y si es diferente de stop sigue ejecutando.
            while(!cadena.equals("stop")){
                bw.write(cadena + "\n");
                //vacia la linea por si hay algo.
                bw.flush();
                //Devuelve el random.
                numeros = procesoSC.nextLine();
                System.out.println(numeros);
                //guarda los numeros en el fichero.
                bw1.write(numeros+"\n");
                cadena = sc.nextLine().toLowerCase();
            }
            bw1.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    }

