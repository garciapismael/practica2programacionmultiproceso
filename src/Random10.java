import java.util.Scanner;

public class Random10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String cadena = sc.nextLine().toLowerCase();

        while(!cadena.equals("stop")){
            System.out.println((int) (Math.random() * 11));
            cadena = sc.nextLine().toLowerCase();
        }
    }
}
